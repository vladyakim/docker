# Docker

To start docker simultaneously PostgreSQL and JIRA Software containers follow the next few steps:

 1. Create a folder named whatever you find suitable, eg. JIRA
 2. Copy the docker-compose.yml file within
 3. Copy config folder along with its content within the newly created folder in step #2
 4. Select version of PostgreSQL and Jira Software
 		- currently the setup is to start certain images that might be changed:
 			JIRA Software image: cptactionhank/atlassian-jira-software:latest
 			PostgreSQL image: postgres:9.6.3
 		
 		Both can be change by any other image available:
 		https://hub.docker.com/_/postgres/	
 		https://hub.docker.com/r/cptactionhank/atlassian-jira-software/
 		
 5. Run docker containers by running: 
 		docker-compose up 
 	while in folder created in step #1
 	
JIRA would be available http://localhost:8081/

You can also connect to DB using pgAdmin tool

NB!!!!! 
	On initial start connecting JIRA to the newly created jira DB use your machine IP/machine name 	
 
 Useful commands:
 	Stop containers:
 	   - docker-compose stop
 	If changes applied in docker-compose.yml:
 	   - docker-compose stop
 	   - docker-compose rm -v
 	  and restart:
 	   - docker-compose up 
 	   
 	   	
 			  		